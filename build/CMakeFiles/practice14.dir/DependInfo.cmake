# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/stb_image.c" "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/build/CMakeFiles/practice14.dir/stb_image.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLM_ENABLE_EXPERIMENTAL"
  "GLM_FORCE_SWIZZLE"
  "PROJECT_ROOT=\"/home/mpereskokova/spbu/sem7/graph/tasks/practice14\""
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../rapidjson/include"
  "../"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/aabb.cpp" "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/build/CMakeFiles/practice14.dir/aabb.cpp.o"
  "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/frustum.cpp" "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/build/CMakeFiles/practice14.dir/frustum.cpp.o"
  "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/gltf_loader.cpp" "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/build/CMakeFiles/practice14.dir/gltf_loader.cpp.o"
  "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/main.cpp" "/home/mpereskokova/spbu/sem7/graph/tasks/practice14/build/CMakeFiles/practice14.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLM_ENABLE_EXPERIMENTAL"
  "GLM_FORCE_SWIZZLE"
  "PROJECT_ROOT=\"/home/mpereskokova/spbu/sem7/graph/tasks/practice14\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../rapidjson/include"
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
